package org.springframework.samples.petclinic.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;

import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;

class VetTest {

	@Test
	@Transactional
	public void testHasMemo() {
		Vet vet1 = new Vet();
		vet1.setLastName("vet1");
		Memo memo1 = new Memo();
		Memo memo2 = new Memo();
		memo1.setDescription("premier memo");
		memo2.setDescription("Deuxième memo");  
		
		assertEquals(0, vet1.getMemos().size());
		vet1.addMemo(memo1);
		vet1.addMemo(memo2);
		assertEquals(2, vet1.getMemos().size());
		List<Memo> memos = new ArrayList<Memo>(vet1.getMemos());
		assertTrue(memos.contains(memo1));
		assertTrue(memos.contains(memo2));
		
	}
	
	@Test
	@Transactional
	public void testHasOperation() {
		Pet miaou = new Pet();
		miaou.setName("Miaou");
		Vet vet = new Vet();
		vet.setLastName("vet1");
		Operation op1 = new Operation();
		op1.setPet(miaou);
		op1.setVet(vet);
		
		assertEquals(0, vet.getOperations().size());
		vet.addOperation(op1);
		assertEquals(1, vet.getOperations().size());
		List<Operation> operation = new ArrayList<Operation>(vet.getOperations());
		assertTrue(operation.contains(op1));
	}

}
