package org.springframework.samples.petclinic.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;

import java.util.ArrayList;
import org.springframework.transaction.annotation.Transactional;

class PetTest {

	@Test
	@Transactional
	public void testHasVisit() {
		Pet miaou = new Pet();
		miaou.setName("Miaou");
		Visit vis1 = new Visit();
		Visit vis2 = new Visit();
		vis1.setDescription("Première visite");
		vis2.setDescription("Deuxième visite");

		
		assertEquals(0, miaou.getVisits().size());
		miaou.addVisit(vis1);
		miaou.addVisit(vis2);
		assertEquals(2, miaou.getVisits().size());
		List<Visit> visite = new ArrayList<Visit>(miaou.getVisits());
		assertTrue(visite.contains(vis1));
		assertTrue(visite.contains(vis2));
		
	}
	
	@Test
	@Transactional
	public void testHasOperation() {
		Pet miaou = new Pet();
		miaou.setName("Miaou");
		Vet vet = new Vet();
		vet.setLastName("vet1");
		Operation op1 = new Operation();
		op1.setPet(miaou);
		op1.setVet(vet);
		
		assertEquals(0, miaou.getOperations().size());
		miaou.addOperation(op1);
		assertEquals(1, miaou.getOperations().size());
		List<Operation> operation = new ArrayList<Operation>(miaou.getOperations());
		assertTrue(operation.contains(op1));
	}

}
