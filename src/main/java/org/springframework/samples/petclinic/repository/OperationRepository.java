package org.springframework.samples.petclinic.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.BaseEntity;
import org.springframework.samples.petclinic.model.Operation;

public interface OperationRepository {

	
    void save(Operation operation) throws DataAccessException;

    List<Operation> findByVetId(Integer vetId);
    List<Operation> findByPetId(Integer petId);
}
