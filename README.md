# Spring

## TP1
### Exercice 2
[Diagramme de classe](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/diagramme_de_classe.PNG)

[Operation.java](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/src/main/java/org/springframework/samples/petclinic/model/Operation.java)

[Pet.java](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/src/main/java/org/springframework/samples/petclinic/model/Pet.java)

[Vet.java](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/src/main/java/org/springframework/samples/petclinic/model/Vet.java)

### Exercice 4
[PetTest.java](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/src/test/java/org/springframework/samples/petclinic/model/PetTest.java)

[VetTest.java](https://gitlab.com/samuel.labagnere1/jee-spring-labagnere-larbalestrier/-/blob/main/src/test/java/org/springframework/samples/petclinic/model/VetTest.java)
